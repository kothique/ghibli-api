const axios = require('axios').create({
  baseURL: 'https://ghibliapi.herokuapp.com/'
})

/**
 * @param {?object}   options
 * @param {?string[]} options.fields
 * @param {?number}   options.limit
 * @return {Promise<object[]>}
 */
module.exports.getFilms = function getFilms(options = {}) {
  const { fields = [], limit } = options;

  return axios.get(`/films`, {
    params: {
      ...fields.length > 0 ? { fields: fields.join(',') } : null,
      ...limit && { limit }
    }
  })
}

/**
 * @param {?object}   options
 * @param {?string[]} options.fields
 * @return {Promise<object>}
 */
module.exports.getFilm = function getFilm(id, options = {}) {
  const { fields = [] } = options;

  return axios.get(`/films/${id}`, {
    params: {
      ...fields.length > 0 ? { fields: fields.join(',') } : null
    }
  })
}
